<!DOCTYPE html>
<html>
<head>
	<title>Coach Wilton Machado</title>
	<!-- fAVICON -->
	<link rel="shortcut icon" href="assets/images/favicon.ico" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0 ">
	
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<!-- Bootstrap.CSS -->
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
    <script src="jquery.js"></script>
	<link href = "https://fonts.googleapis.com/css?family= Lobster " rel = "stylesheet">

</head>
<body>

	<header>
		<div class="content">
			<div class="redesocial">
				<ul>
					<a href="https://www.facebook.com/wiltonmachadocoach/" target="_blank">
						<li>
							<img src="assets/images/facebook_16x16.png">
						</li>
					</a>
					<a href="https://www.instagram.com/mr.wil2015/" target="_blank"><li>
						<img src="assets/images/instagram_16x16.png">
					</li></a>
				</ul>
			</div>
			<div class="contact">
				<a href="#"><img src="assets/images/cell_16x16.png"></a>
				<a href="tel:22974001177"><span>22 97400-1177</span></a>
				<a href="#"><img class="none" src="assets/images/email_16x16.png"></a>
				<a class="none" href="mailto:contato@wiltonmachado.com">contato@wiltonmachado.com.br</a>
			</div>
		</div>
		<div class="content1">
			<div class="text">
				<h1>COACH</h1>
				<h2>WILTON MACHADO</h2>
				<h2>VOCÊ PODE IR ALÉM</h2>
			</div>
			<div class="image">
				<img src="assets/images/perfil.jpg">
			</div>
		</div>
		<div class="slide2">
			<div class="conteudo">
				<iframe class="socialativo" width="280" height="160" src="https://www.youtube.com/embed/hcRPGwP3HCc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				<iframe class="socialativo2" width="640" height="360" src="https://www.youtube.com/embed/hcRPGwP3HCc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>
		</div>
	</header>
	<section>
		<div class="ebook">
			<div class="left">
				<img src="assets/images/ebook.png" width="100" height="100">
				<p>Este guia definitivo reúne a base de todas as minhas estratégias de marketing para alavancar vendas ou criar um negócio 100% digital do zero</p>
			</div>


			<div class="right">
				<div class="item">
					<p>DIGITE SEU MELHOR E-MAIL E BAIXE O SEU E-BOOK GRATUITAMENTE</p>
					<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
					<form action="https://wiltonmachado.us17.list-manage.com/subscribe/post?u=a9fd1f4c1acef5570e565d2c0&amp;id=328b10c194" method="post" target="_blank">
						<input type="email" value="" name="EMAIL" placeholder="Digite o seu melhor e-mail"  required>
						<input class="color" value="DOWNLOAD NOW" type="submit" name="subscribe" placeholder="DOWNLOAD NOW" >
					</form>
				</div>
			</div>
		</div>
	</section>
	<article>
	
		<div class="sobre">

			<div class="on">
				<h2 class="one">CONHEÇA O <span>WILTON</span> MACHADO</h2>
				<img src="assets/images/perfil.jpg">
			</div>
			
			<div>
				<h2 class="two">CONHEÇA O <span>WILTON</span> MACHADO</h2>
				<p>Wilton Machado  é professor há mais de 20 anos, Coach, palestrante, gestor de escola pública. Já treinou várias pessoas  ajudando elas  a desenvolverem suas habilidades para terem sucesso profissional e pessoal.<br>
				A missão de vida do Wilton é fazer as pessoas acreditarem que podem ir além e efetivamente caminharem na direção disso para a vida delas. Cada treinamento ou sessão de coach que o Wilton oferece tem exatamente o propósito de levar cada um para o próximo nível. Inspire-se para ir sempre além.</p>
			</div>
			

		</div>
		<div class="box1">
			<p>"Você pode ser o que quiser, basta querer ser!"</p>
			<h2>Assista o vídeo e saiba como <br><span>FAZER ACONTECER</span></h2>
			<div>
				<img src="assets/images/seta.png" width="40">
			</div>
			
		</div>
		<div class="box2">
			<iframe class="videoativo" width="280" height="160" src="https://www.youtube.com/embed/F4fyKnu5dt0?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

			<iframe class="videoativo2" width="560" height="315" src="https://www.youtube.com/embed/Mcvg9shs63I?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

		</div>
		<div class="social">
			<div class="box3">
				<h2>SIGA O <span>WILTON</span> NO FACEBOOK</h2>
				<p>E receba grátis sacadas de empreendedorismo e coach</p>
				<iframe class="socialativo" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fwiltonmachadocoach%2F&tabs&width=300&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=374976069585432" width="300" height="214" style="border:none;overflow:hidden; margin: auto;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

				<div>
				<iframe class="socialativo2" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fwiltonmachadocoach&tabs&width=600&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=374976069585432" width="600" height="300" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
			</div>
		</div>
	</article>
	<section>
		<div class="contato">
			<div class="box4">
				<h3>CONTATO</h3>
				<p style="margin-top: 10px;">Precisa elevar sua performance?</p>
				<p style="margin-bottom: 10px;">Faça contato agora</p>
				<a href="tel:22974001177"><span>Contato:</span> 22 97400-1177</a>
				<a href="mailto:contato@wiltonmachado.com"><span>E-mail: </span>contato@wiltonmachado.com.br</a>
				<p style="margin-top: 10px;">Envie uma mensagem através do formúlario</p>
			</div>
		</div>
		<div class="contato2">
			<div class="box5">
				<?php
				  if(isset($_POST['NOME']) && !empty($_POST['NOME'])){

				    $NOME = addslashes($_POST['NOME']);
				    $EMAIL = addslashes($_POST['EMAIL']);
				    $TEXT = addslashes($_POST['TEXT']);

				    $para = "contato@wiltonmachado.com.br";
				    $assunto = "Treinamento - Palestra - Coach";
				    $corpo = "Nome: ".$NOME." - E-mail: ".$EMAIL." - Mensagem: ".$TEXT;

				    $cabecalho = "From: contato@wiltonmachado.com.br"."\r\n".
				    			"Reply-To: ".$EMAIL."\r\n".
				    			"X-Mailer: PHP/".phpversion();

					if (mail($para, $assunto, $corpo, $cabecalho)) {
						echo "<h2>EMAIL ENVIADO, OBRIGADO! <br>
								EM BREVE ENTRAREMOS EM CONTATO.</h2>";
					    exit;
					}else {
						echo "<h2>Não foi enviado, tente novamente!</h2>";
					};
				};
				?>
				<!-- action="https://wiltonmachado.us17.list-manage.com/subscribe/post?u=a9fd1f4c1acef5570e565d2c0&amp;id=0bd09b871f" -->
				<form target="_blank" method="post" >
					<input type="text" name="NOME" required placeholder="Nome">
					<input type="email" name="EMAIL" required placeholder="E-mail">
					<textarea  placeholder="Mensagem" name="TEXT"></textarea>
					<input class="enviar" type="submit" name="subscribe">	
				</form>
			</div>
		</div>
	</section>
	 <footer>
    	<div class="footer-top">
	        <ul class="socials">
	            <li class="facebook">
	                <a href="https://www.facebook.com/wiltonmachadocoach/" data-hover="Facebook" target="_black">Facebook</a>
	            </li>
	            <li class="instagram">
	            	<a href="https://www.instagram.com/wiltonmachadocoach/" data-hover="instagram" target="_black">Instagram</a>
	            </li>
	            <li class="gplus">
	                <a href="mailto:coachwiltonmachado@gmail.com" data-hover="Google +" target="_black">Google +</a>
	            </li>
	        </ul>
    	</div>
    	<div class="footer_div">
	    	<div class="rodapeint" >
	    		<img src="assets/images/logotipo.png" width="150" height="150">
	    	</div>
    	</div>
    	<div class="copyright" >
	    	<span class="creditos">&copy; Wilton Machado. All Rights Reserved</span>
	    </div>
	    <a href="" class="voltar-ao-topo">Voltar ao topo</a>
    </footer>
</body>
</html>

